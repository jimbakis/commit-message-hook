#!/usr/bin/env bats

function teardown() {
    echo "" > data/commit-msg.txt
}

@test "test fail on subject line size length" {
    echo "fix: A commit subject that's more than 50 charactes long" > data/commit-msg.txt
    
    run ../commit-msg data/commit-msg.txt
    [ "$status" -eq 1 ]
    [ "$output" = "Commit header's length should not longer than 50 characters." ]
}

@test "test fail on subject line prefix" {
    echo "badprefix: Commits subject" > data/commit-msg.txt
    
    run ../commit-msg data/commit-msg.txt
    [ "$status" -eq 1 ]
    [ "$output" = "Commit header should start with one of feat, fix, docs, style, refactor, test, chore followed by $(:) and a space." ]
}

@test "test fail on subject line ending" {
    echo "fix: Commit subject." > data/commit-msg.txt
    
    run ../commit-msg data/commit-msg.txt
    [ "$status" -eq 1 ]
    [ "$output" = "Commit header should end with period." ]
}

@test "test fail on subject line capitalization" {
    echo "fix: commit without capitalizing first letter" > data/commit-msg.txt
    
    run ../commit-msg data/commit-msg.txt
    [ "$status" -eq 1 ]
    [ "$output" = "Commit subject line should be capitalized." ]
}

@test "test fail on empty line between header and subject" {
    echo "fix: Commits subject
not empty line
other line
" > data/commit-msg.txt
    
    run ../commit-msg data/commit-msg.txt
    [ "$status" -eq 1 ]
    [ "$output" = "Commit header should be followed by a black line" ]
}

@test "test fail on body's lines size length" {
    echo "fix: Commits subject

With a very long line in body which exceed the maximum of 72 characters per line
" > data/commit-msg.txt
    
    run ../commit-msg data/commit-msg.txt
    [ "$status" -eq 1 ]
    [ "$output" = "Commit body's length should not be longer than 72 characters." ]
}

@test "test success on optional body" {
    echo "fix: Commits subject
" > data/commit-msg.txt
    
    run ../commit-msg data/commit-msg.txt
    [ "$status" -eq 0 ]
}

@test "test success on optional body with comments" {
    echo "fix: Commits subject
# This is a comment
# This is another comment
" > data/commit-msg.txt

    run ../commit-msg data/commit-msg.txt
    [ "$status" -eq 0 ]
}

@test "test success on body with comments" {
    echo "fix: Commits subject

# This is a comment
This is body description
# This is a comment with a very long line in body which exceed the maximum of 72 characters per line
# This is another comment with a very long line in body which exceed the maximum of 72 characters per line
This is body description
" > data/commit-msg.txt

    run ../commit-msg data/commit-msg.txt
    [ "$status" -eq 0 ]
}

@test "test success" {
    echo "fix: Commits subject

A description about the commit
" > data/commit-msg.txt
    
    run ../commit-msg data/commit-msg.txt
    [ "$status" -eq 0 ]
}
