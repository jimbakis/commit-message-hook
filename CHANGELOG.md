# Change Log

## [1.2] - 2020-08-27

### Fixed

- Issue on comments in body (See #2)

## [1.1] - 2020-08-24

### Fixed

- Issue on optional body with comments (See #1)

## [1.0] - 2020-08-23

### Added

- Commit-msg hook implementation
