#!/bin/bash

COMMIT_FILE=$1
LINE_INDEX=0
HEADER_PREFIX_REGEXP="^build: |^chore: |^ci: |^docs: |^feat: |^fix: |^perf: |^refactor: |^style: |^test: "

while IFS= read -r line
do
    COMMIT_LINES[${LINE_INDEX}]="$line"
    LINE_INDEX=$LINE_INDEX+1
done < "$COMMIT_FILE"

if [ ${#COMMIT_LINES[0]} -gt 50 ]; then
    echo "Commit header's length should not longer than 50 characters."
    exit 1
fi

if ! [[ ${COMMIT_LINES[0]} =~ $HEADER_PREFIX_REGEXP ]]; then
  echo "Commit header should start with one of feat, fix, docs, style, refactor, test, chore followed by $(:) and a space."
  exit 1
fi

if [[ ${COMMIT_LINES[0]} =~ \.$ ]]; then
  echo "Commit header should end with period."
  exit 1
fi

if ! [[ ${COMMIT_LINES[0]#*: } =~ ^[A-Z] ]]; then
    echo "Commit subject line should be capitalized."
    exit 1
fi

if [ "${COMMIT_LINES[2]}" ] && [ ${#COMMIT_LINES[1]} -gt 0 ] && ! [[ ${COMMIT_LINES[1]} =~ ^# ]] && ! [[ ${COMMIT_LINES[1]} =~ ^# ]]; then
    echo "Commit header should be followed by a black line"
    exit 1
fi

for (( i = 2; i < LINE_INDEX; i++ )); do
    if ! [[ "${COMMIT_LINES[i]}" =~ ^# ]] && [ ${#COMMIT_LINES[i]} -gt 72 ]; then
        echo "Commit body's length should not be longer than 72 characters."
        exit 1
    fi
done

exit 0