# Commit-msg hook

Commit-msg hook is an implementation for Git's corresponding hook ([link]).

[link]: https://git-scm.com/docs/githooks#_commit_msg

This implementation accepts a commit message if it mets the following conditions:
1. Commit header length should not be more than 50 characters
2. Commit header should be prefixed with one of the following: `build: `, `chore: `, `ci: `, `docs: `, `feat: `, 
    `fix: `, `perf: `, `refactor: `, `style: `, `test: `
3. Commit header should not end with period (`.`)
4. Commit header should start with a capital letter
5. If there is a commit body, there should be a black line between header and body
6. Commit body line length should not be more than 72 characters

## Usage

### Manual
1. Copy contents of `commit-msg` to a new file
2. Save file as `commit-msg`
3. Make file executable:
```bash
chmod +x commit-msg
```
4. Copy file to a git repo
```bash
cp commit-msg /path/to/repo/.git/hooks
```

### Using curl
In a git folder run:
```bash
curl https://gitlab.com/jimbakis/commit-message-hook/-/raw/master/commit-msg?inline=false > .git/hooks/commit-msg && chmod +x .git/hooks/commit-msg
```

### Note
if you are having issues on committing with an error like:
`hint: Waiting for your editor to close the file... error: There was a problem with the editor 'vi'.
Please supply the message using either -m or -F option.
`
please run:
```bash
git config --global core.editor /usr/bin/vim
```
